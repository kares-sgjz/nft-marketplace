## 体系结构和客户端流

<p align="center">
  <img alt="Client-Flow"src="https://res.cloudinary.com/nataliebravo/image/upload/v1626701427/NFT/client-side-flow_iqhq9a.png">
<p />

<p align="center">
  <img alt="Architecture"src="https://res.cloudinary.com/nataliebravo/image/upload/v1626701440/NFT/arquitechure_hunzuw.png">
<p />


<a id='technologies'/>

## :gear: 运行环境

需要如下技术

#### **前端** <sub><sup>React + JavaScript</sup></sub>
  - [React](https://pt-br.reactjs.org/)
  - [Axios](https://github.com/axios/axios)
  - [Redux](https://redux.js.org/)
  - [Web3.js](https://web3js.readthedocs.io/en/v1.3.4/)
  - [Material UI](https://material-ui.com/pt/)

#### **后端** <sub><sup>Express</sup></sub>
  - [Express](https://expressjs.com/pt-br/)

#### **区块链和智能合约** <sub><sup>Solidity</sup></sub>
  - [Solidity](https://docs.soliditylang.org/)
  - [Truffle](https://www.trufflesuite.com/)
  - [Ganache](https://www.trufflesuite.com/ganache)


<a id='how-to-use'/>

## :joystick: 运行

运行环境
* [Git](https://git-scm.com)
* [Node](https://nodejs.org/)
*  [npm](https://www.npmjs.com/)
* [Truffle](https://www.trufflesuite.com/)
* [Ganache](https://www.trufflesuite.com/ganache)
* 克隆如下地址:
  * https://gitee.com/waj499535466/nft-marketplace.git


进入文件夹并且运行:


```bash
$ cd NFT-Marketplace

# install the dependencies
$ npm install

# run ganache
$ ganache-cli(可视化ganache软件也可以)
建议可视化ganache： 创建工作环境  配置端口 8545 添加truffle-config.js到工作环境即可

# deploy de contracts on the blockchain
$ truffle migrate

# run the client-side
$ cd client
$ npm install
$ npm start

# run the backend
$ cd backend
$ npm install
$ npm start
```

