import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import TextField from '@material-ui/core/TextField';
import { api } from "../../services/api";

const styles = {
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  account: {
    marginLeft: "auto",
    display: "flex",
    alignItems: "center",
    
  },
  button: {
    color: 'white'
  },
  field: {
    marginTop: 20,
    marginBottom: 20,
    display: 'block'
}
};

class TemporaryDrawer extends React.Component {
  state = {
    right: false,   // 控制向右开关的状态变量
    words: '',     // 用户输入的助记词
    address: localStorage.getItem('address')?localStorage.getItem('address'):"" // 地址初始化 从硬盘中读取地址 获取不到则设置为空
  };
  // 控制抽屉开关函数
  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };
  // 获得助记词 并且存到状态变量中
  inputchange = (e) =>{
    // console.log("123",e)
    this.setState({
      words:e.target.value
    })
  }
  // 提交助记词函数
  login () {
    const data = new FormData();
    const words = this.state.words;
    // 将助记词 以key-value 方式存到请求体中
    data.append('words', words)
    // 通过axios 构造请求 请求后端接口
    api.post("/getAddress", data, {
      headers: {
        'Content-Type': `multipart/form-data; boundary=${data._boundary}`
      },
    }).then(res=>{
      console.log(res.data.address)
      // 获取地址后 存到硬盘上
      localStorage.setItem('address', res.data.address)
      // 更新状态变量 收起抽屉
      this.setState({
        address: res.data.address,
        right: false
      })
    })
  }
  render() {
    const { classes } = this.props;
    
    const handleSubmit = (e) => {
        e.preventDefault()

        if(this.title){
            console.log(this.title)
        }
    }


    const sideList = (
      <div className={classes.list}>
        
       <form noValidate autoComplete='off' onSubmit={handleSubmit}>
        <TextField
        onChange={
        this.inputchange
       }
        className={classes.field}
        label = "助记词"
        variant='outlined'
        color='secondary'
        fullWidth
        required
        />
        <Button 
        onClick={() => this.login()}
        type="submit"
        color='secondary'
        variant='contained'>
            提交
        </Button>

            
       </form>
      </div>
    );

    return (
        
      <div className={classes.account}>
      <Button variant="contained" color="primary" onClick={this.toggleDrawer('right', true)} startIcon={ <AccountBalanceWalletIcon titleAccess="Wallet Address" className={classes.walletIcon}/>}>
      {this.state.address.slice(0,7)+'...'+this.state.address.slice(-4)}
      </Button>
        <Drawer anchor="right" open={this.state.right} onClose={this.toggleDrawer('right', false)}>
          <div
            tabIndex={0}
            role="button"
          >
            {/* 抽屉中的内容组件 */}
            {sideList}
          </div>
        </Drawer>
      </div>
    );
  }
}

TemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TemporaryDrawer);