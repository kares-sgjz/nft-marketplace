import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import TextField from '@material-ui/core/TextField';
import Typography from "@material-ui/core/Typography";
import CssBaseline from "@material-ui/core/CssBaseline";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Drawer from '@material-ui/core/Drawer';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import TemporaryDrawer from './drawer'

import {useStyles} from './styles.js'

import logo from '../../assets/Logo.svg';

const Header = () => {
  const classes = useStyles();
  const account = useSelector((state) => state.allNft.account);

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar className={classes.header}>
        <Toolbar>
          <Link to="/">
            <img src={logo} alt="Galerie" className={classes.logo}/>
          </Link>
          {/* 应用material-ui中的组件  */}
          <TemporaryDrawer> </TemporaryDrawer>
        </Toolbar>
        
      </AppBar>
      <Toolbar />
    </React.Fragment>
  );
};

export default Header;
