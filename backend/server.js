const express = require('express');
var cors = require('cors');
var bodyParser = require("body-parser");
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/images', express.static('imgs')); // 直接访问静态文件 http://localhost:3000/images/kitten.jpg
const routes = require('./routes')(app);

const server = app.listen(3333, () => {
  console.log('Listening on port %s...', server.address().port);
});
