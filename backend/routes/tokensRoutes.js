const fs = require('fs');
const path = require('path');

const { v4: uuidv4 } = require('uuid');
const upload = require('../config/uploadConfig');


const {bfmetaSDK} = require('./util')

const tokensRoutes = (app) => {
  // VARIABLES
  const dbFile = path.resolve(__dirname, 'db.json');
  const db = fs.readFileSync(dbFile, 'utf8');
  // const db = require('./db.json');
  const tokens = JSON.parse(db);

  // INDEX
  app.get('/tokens/:tokenID', (req, res) => {
    const { tokenID } = req.params;

    res.status(200).json(tokens[tokenID]);
  });

  // CREATE
  app.post('/tokens', upload.single('img'), (req, res) => {
    console.log(req.body)
    const { filename } = req.file;
    const { tokenId, name, description } = req.body;

    // const tokenId = uuidv4();

    tokens[tokenId] = {
      name,
      description,
      image: req.protocol + '://' + req.get('host') + "/images/" + filename
    };

    fs.writeFileSync(dbFile, JSON.stringify(tokens));

    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl + '/' + tokenId;

    res.status(201).json({ message: fullUrl });
  });
  // 获取地址接口
  app.post('/getAddress',upload.single('img'), (req, res)=>{
    // 调用sdk相关函数 通过助记词获得地址
    bfmetaSDK.bfchainSignUtil.createKeypair(req.body.words).then(re=>{
      bfmetaSDK.bfchainSignUtil.getAddressFromPublicKey(re.publicKey).then(re=>{
        res.status(201).json({ address: re });
      })
    })
    
  });

};

module.exports = tokensRoutes;

