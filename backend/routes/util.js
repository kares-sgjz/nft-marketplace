const { BFMetaSDK } = require ("@bfmeta/node-sdk")
const { CryptoHelper} =  require ("@bfmeta/node-sdk/build/test/helpers/cryptoHelper");
const cryptoHelper = new CryptoHelper();
const config = {
    node: {
        /**节点 ip, 默认值 [127.0.0.1] */
        ip: "127.0.0.1",
        /**节点端口号, 默认值 9003 */
        port: 9003,
    },
    //  "请求超时时间, 单位 ms, 默认 10000",
    requestTimeOut: 10000,
    // "请求协议, http || websocket, 默认值 websocket",
    requestProtocol: 'websocket',
};
const bfmetaSDK = new BFMetaSDK({ netType: "testnet", cryptoHelper}, config);
module.exports.bfmetaSDK = bfmetaSDK
